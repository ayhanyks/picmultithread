/*
 * File:   newmain.c
 * Author: ayhan
 *
 * Created on 30 September 2019, 22:34
 */

// CONFIG1H
#pragma config FOSC = INTIO67   // Oscillator Selection bits (Internal oscillator block)
#pragma config PLLCFG = OFF     // 4X PLL Enable (Oscillator used directly)
#pragma config PRICLKEN = ON    // Primary clock enable bit (Primary clock enabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

// CONFIG2L
#pragma config PWRTEN = OFF     // Power-up Timer Enable bit (Power up timer disabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
#pragma config BORV = 190       // Brown Out Reset Voltage bits (VBOR set to 1.90 V nominal)

// CONFIG2H
#pragma config WDTEN = OFF      // Watchdog Timer Enable bits (Watch dog timer is always disabled. SWDTEN has no effect.)
#pragma config WDTPS = 32768    // Watchdog Timer Postscale Select bits (1:32768)

// CONFIG3H
#pragma config CCP2MX = PORTB3  // CCP2 MUX bit (CCP2 input/output is multiplexed with RB3)
#pragma config PBADEN = ON      // PORTB A/D Enable bit (PORTB<5:0> pins are configured as analog input channels on Reset)
#pragma config CCP3MX = PORTB5  // P3A/CCP3 Mux bit (P3A/CCP3 input/output is multiplexed with RB5)
#pragma config HFOFST = ON      // HFINTOSC Fast Start-up (HFINTOSC output and ready status are not delayed by the oscillator stable status)
#pragma config T3CMX = PORTC0   // Timer3 Clock input mux bit (T3CKI is on RC0)
#pragma config P2BMX = PORTB5   // ECCP2 B output mux bit (P2B is on RB5)
#pragma config MCLRE = EXTMCLR  // MCLR Pin Enable bit (MCLR pin enabled, RE3 input pin disabled)

// CONFIG4L
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config LVP = OFF        // Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

// CONFIG5L
#pragma config CP0 = OFF        // Code Protection Block 0 (Block 0 (000800-003FFFh) not code-protected)
#pragma config CP1 = OFF        // Code Protection Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP2 = OFF        // Code Protection Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP3 = OFF        // Code Protection Block 3 (Block 3 (00C000-00FFFFh) not code-protected)

// CONFIG5H
#pragma config CPB = OFF        // Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-protected)
#pragma config CPD = OFF        // Data EEPROM Code Protection bit (Data EEPROM not code-protected)

// CONFIG6L
#pragma config WRT0 = OFF       // Write Protection Block 0 (Block 0 (000800-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection Block 3 (Block 3 (00C000-00FFFFh) not write-protected)

// CONFIG6H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)

// CONFIG7L
#pragma config EBTR0 = OFF      // Table Read Protection Block 0 (Block 0 (000800-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)

// CONFIG7H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-0007FFh) not protected from table reads executed in other blocks)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include "simpleMultiThread.h"
#include "rs485.h"





#define LED        LATCbits.LATC0
#define DIG1       LATBbits.LATB0
#define DIG2       LATBbits.LATB1
#define DIG3       LATBbits.LATB2
#define dispa      LATAbits.LATA0

#define _XTAL_FREQ 16000000

typedef  unsigned char u8;
typedef  unsigned int  u16;
typedef int i16;
typedef char i8;

static char tmp[32];



typedef struct{
    u16 entryPoint;
    u8 status;
    u16 stack[10];        
    
    
}pthrd;



void thrdA(){
    
    unsigned char x=0;
    static unsigned char y=0;
    //rs485_print_string("THRDA START:\n\r");
    while(1){
        hex2ascii(y,tmp);
        rs485_print_string("THRDA:");
        rs485_print_string(tmp);
        rs485_print_string("\n\r");
        x++;
       y++;
       //__delay_ms(1000); 
        threadSleepms(10000);
    }
    
}

void thrdB(){
    unsigned char x=0;
    static unsigned char y=0;
    //rs485_print_string("THRDA START:\n\r");
    while(1){
        hex2ascii(y,tmp);
        rs485_print_string("THRDB:");
        rs485_print_string(tmp);
        rs485_print_string("\n\r");
        x++;
        y++;
       //__delay_ms(1000); 
        threadSleepms(10000);
    }
    
}

void thrdC(){
    unsigned char x=0;
    static unsigned char y=0;
    //rs485_print_string("THRDA START:\n\r");
    while(1){
        hex2ascii(y,tmp);
        rs485_print_string("THRDC:");
        rs485_print_string(tmp);
        rs485_print_string("\n\r");
        x++;
        y++;
       //__delay_ms(1000); 
        threadSleepms(20000);
    }
    
}









void main(void) {
    unsigned char i;
    int test1=0;
    int test2=1;
    int test3=0;
    
    OSCCONbits.IRCF=0b111;
    
    ANSELB=ANSELB & 0xF0; //b0 b1 b2 b3 digital
    ANSELA=0x00;
     
     
    TRISCbits.TRISC0=0;
    
    TRISBbits.TRISB0=0;
    TRISBbits.TRISB1=0;
    TRISBbits.TRISB2=0;
    
    TRISAbits.TRISA0=0;
   
    DIG1=0;
    DIG2=0;
    DIG3=0;
    dispa=0;
    
    init_RS485();
    i=0;
    
    simpleThreadInit();
     
    createThread(&thrdA,0,0);
    createThread(&thrdB,0,0);
    createThread(&thrdC,0,0);
    rs485_print_string("HELLO WORLD\n\r");
    
    threadRunner();
    
    test1=0;
    test2=0;
    test3=0;
    
    rs485_print_string("HELLO WORLD\n\r");
 
    
    dispa=1;
    
#if 0
    while(test1+test2+test3){
    rs485_print_string("HELLO WORLD\n\r");

    DIG1=1;kml
     __delay_ms(50);        
    DIG1=0;
     __delay_ms(50); 
     
     i++;
     hex2ascii(i,tmp);
     rs485_print_string(tmp);
     
    } 
#endif
    thrdA();
    thrdB();
    thrdC();
    return;
}
    

u16 tmr2cnt=0;
u8 rcrec;
u16 thrdAddr;

void __interrupt(low_priority) CheckButtonPressed(void)
{
    
      
    if(PIR1bits.RC1IF)
    {
        //  LED=~LED;
        //RS485_receive(RCREG1);
        rcrec=RCREG1;
        
        
    }
    
    if (INTCONbits.TMR0IF)
    {
        INTCONbits.TMR0IF=0;
        
      }
     if(PIR1bits.TMR2IF)
    {
        
            PIR1bits.TMR2IF=0;

            tmr2cnt++;
            
            if(!(tmr2cnt & 0x3FF))
            {
                
               //~3 ms de bir tekrarlanacak
               dispa=1-dispa;
               //todo: 
               // set stack pointer to other function 
               //we can be in any thread
               //1. while 1 thread: control thread
               //2. thrd1
               //3. thrd2
               //4. thrd3..
               // any thread..
               /*
                rs485_print_string("stack:");
                hex2ascii((BSRS & 0x0F),tmp);  rs485_print_string(tmp);
                hex2ascii((STKPTR & 0x0F),tmp);
               
                rs485_print_string("\n\r");
                rs485_print_string("TOS:"); 
                hex2ascii(TOSU,tmp);  rs485_print_string(tmp);
                hex2ascii(TOSH,tmp);  rs485_print_string(tmp);  
                hex2ascii(TOSL,tmp);  rs485_print_string(tmp);
                rs485_print_string("\n\r");
                
                
                
                //1. save the current stack values to variable
                thrdAddr=(int)(thrdB);
                
                TOSH=thrdAddr>>8;
                TOSL=thrdAddr&0xFF;
                
              
                rs485_print_string("\n\r");
                rs485_print_string("TOS:"); 
                hex2ascii(TOSU,tmp);  rs485_print_string(tmp);
                hex2ascii(TOSH,tmp);  rs485_print_string(tmp);  
                hex2ascii(TOSL,tmp);  rs485_print_string(tmp);
                rs485_print_string("\n\r");
                
                
                
                rs485_print_string("Stack:");
                hex2ascii((STKPTR & 0x0F),tmp);  rs485_print_string(tmp);
                rs485_print_string("\n\r");
                
           //     PIE1bits.TMR2IE=0;
                */
            }
            

    }
    
    
}