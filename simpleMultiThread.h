#ifndef SIMPLEMULTIT_
#define SIMPLEMULTIT_

#define MAXSTACKSIZE    32     //todo: create in heap or sth more clever
#define MAXTHREADS   5

typedef  unsigned char u8;
typedef  unsigned int  u16;
typedef int i16;
typedef char i8;
typedef int func(void);


typedef struct {

    unsigned int threadStatus;
    unsigned int mystack[MAXSTACKSIZE];
    unsigned int mystackCounter;
    unsigned int wakeupTime;    
    void*entryPoint;
    void (*entryPointpic)();
    void*input;
    void*output;
    
}simpleThread;

void simpleThreadInit();
void threadSleepms(unsigned int ms);
int createThread(void (*threadFunc)(), void*inputs, void*output);
void threadRunner();

#endif





