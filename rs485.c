/*
 * File:   rs485.c
 * Author: ayhan
 *
 * Created on 05 October 2019, 00:31
 */


#include <xc.h>
#include "rs485.h"

void uart_print_char(char c){
    
  //  return;
    
    while(!TXSTA1bits.TRMT);
    RS485_DE=1;
    TXREG=c;
    while(!TXSTA1bits.TRMT);
    RS485_DE=0;
    
}

void rs485_print_string(char*string){
    
    while(!TXSTA1bits.TRMT);
    RS485_DE=1;
    
    while(*string)
    {
        while(!TXSTA1bits.TRMT)
            ;
        
         TXREG=*string;
         string++;
    }
    while(!TXSTA1bits.TRMT);
    RS485_DE=0;
    
}


//RS485 ayarlanir 9600 baud 8N1
//hem port0 hem port 1 icin
void init_RS485()
{
    
    
    ANSELCbits.ANSC6=0;
    ANSELCbits.ANSC7=0;
    
    TRISCbits.TRISC6=0;    //Make RC6 an output
  
#ifdef KART102
    SPBRG1=71; //9600 baud @11.0592MHz    // Yavasda 0x19 olacak VERSIONDEGIS
#elif defined KART101
    SPBRG1=25;
#elif defined KART1802
    SPBRG1=103;
#endif

     
    TXSTA1bits.TXEN=1;  //Enable transmit
    TXSTA1bits.BRGH=1;  //Select high baud rate
    TXSTA1bits.SYNC=0;
    
    RCSTA1bits.SPEN=1;  //Enable Serial Port
    RCSTA1bits.CREN=1;  //Enable continuous reception
    
    PIR1bits.RC1IF=0;
    PIE1bits.RC1IE=1;

   
     TRISCbits.TRISC5=0; //RS485 out
 
     RS485_DE=0;
    
    
    
}

void hex2ascii(unsigned char hex, char*ascii)
{
    
    ascii[0]=((hex&0xF0)>>4);
    
    if(ascii[0]<=9)
        ascii[0]=ascii[0]+0x30;
    else
        ascii[0]=ascii[0]+0x37;
    
    ascii[1]=hex & 0x0F;
    
    if(ascii[1]<=9)
        ascii[1]=ascii[1]+0x30;
    else
        ascii[1]=ascii[1]+0x37;
   
    ascii[2]=0;
    
    
}
