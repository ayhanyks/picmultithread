#include "simpleMultiThread.h"
#include "stdio.h"
#include <time.h>
#include "xc.h"
#include "rs485.h"

simpleThread myThreads[MAXTHREADS];
simpleThread*currentThread=0;

void fun() 
{ 
    rs485_print_string("fun:fun\n\r");
    return;
} 

void fun2() 
{ 
    rs485_print_string("fun:fun2\n\r");
    return;
} 

void (*fun_ptr)() = &fun; 


unsigned int currentThrnum=0;
#define printf(...) 

void simpleThreadInit(){

    for(int i=0; i<MAXTHREADS;i++)
    {
        myThreads[i].threadStatus=0;    //not initialized..
    }

    currentThread=0;

}

void threadSleepms(unsigned int ms){

    currentThread->wakeupTime=ms;
    
    //save current thread..
    //so that no interrupt call has happened, only saving the TOH_TOL addresses and popping until the stack is in old position.. 
   
    
    threadRunner();

}
    
int createThread(void (*threadFunc)(), void*inputs, void*output){

    int thrnum=0;

    for(thrnum=0; thrnum<MAXTHREADS;thrnum++)
        if( myThreads[thrnum].threadStatus==0)
            break;


    if(thrnum==MAXTHREADS)
        return -1;
    
    myThreads[thrnum].threadStatus=1;
    myThreads[thrnum].mystackCounter=0;
    //myThreads[thrnum].entryPoint=threadFunc;
    myThreads[thrnum].entryPointpic=threadFunc;
    
    myThreads[thrnum].input=inputs;
    myThreads[thrnum].output=output;    


    
    return 0;
}
static char tmp[10];

void printStackAndTOS(const char*initialStr, u8 spval, u8 th, u8 tl){
    
    rs485_print_string(initialStr);
    
    rs485_print_string("\n\rstack:");
    hex2ascii((spval & 0x1F),tmp);  rs485_print_string(tmp);
    hex2ascii((spval & 0x1F),tmp);

    rs485_print_string("\n\r");
    rs485_print_string("TOS:"); 
    hex2ascii(th,tmp);  rs485_print_string(tmp);  
    hex2ascii(tl,tmp);  rs485_print_string(tmp);
    rs485_print_string("\n\r");
}

unsigned int spval;
//register void *sp asm ("STKPTR");
#define sp (STKPTR & 0x0F)

unsigned int mystack0, mystack1;

unsigned int thrnum=0;


void  threadRunner(){
    
    //printStackAndTOS("threadRunner:",STKPTR,TOSH, TOSL);
   
    
    
    if(currentThread==0)
    {
        //call the first available thread
           
        for(thrnum=0; thrnum<MAXTHREADS;thrnum++)
            if( myThreads[thrnum].threadStatus==1)
                break;

        currentThread=&(myThreads[thrnum]);
        currentThrnum=thrnum;
        
    //    rs485_print_string("P starting thread");
    //    hex2ascii(thrnum,tmp);  rs485_print_string(tmp);
    //    rs485_print_string("\n\r");
    
        //      printf("currentThread=%p\n",currentThread->entryPoint );
        mystack0=sp;
  //      printf("stack is at 0x%08x currentThread is 0x%08x \n",sp,currentThread);
        fun_ptr=(void*)currentThread->entryPointpic;
        
        //stack position:X
        
        
        fun_ptr();
        //((void(*)(void))currentThread->entryPoint)();
      //  currentThread->entryPoint();
    }
    else
    {
        
        
         
      //   rs485_print_string("rewoke threadRUnner\n\r");
         currentThread->mystackCounter=0;
         
         while((unsigned int)sp>(unsigned int)mystack0){
            currentThread->mystack[currentThread->mystackCounter++]=(TOSH<<8) | TOSL;
            asm("POP");
            
            
          //  printStackAndTOS("threadRunner2:",STKPTR,TOSH, TOSL);
       
         }   
         //switch to another thread..
            
            thrnum=currentThrnum;
            while(1)
            {
            //   printf("%d->%d [%d]\n",thrnum,myThreads[thrnum].threadStatus,myThreads[thrnum].mystackCounter);
               thrnum++;
               thrnum=(thrnum<MAXTHREADS)?thrnum:0;

               if( myThreads[thrnum].threadStatus==1)
            
                   #if 1            
                        if(!myThreads[thrnum].mystackCounter)
                        {
                            //just start it!
                            currentThread=&(myThreads[thrnum]);
                            currentThrnum=thrnum;
                            mystack0=sp;
                     //        printf("starting threadX %d\n",thrnum);
                     //       while(1);
                            
                        //    rs485_print_string("P starting thread");
                        //    hex2ascii(thrnum,tmp);  rs485_print_string(tmp);
                        //    rs485_print_string("\n\r");
                            fun_ptr=(void*)currentThread->entryPointpic;
                            fun_ptr();
                            //((void(*)(void))currentThread->entryPoint)();
                        }
                        else
                    #endif
                        {  
                            
#if 1
                        // printf("%d\n",thrnum);
                        // curTime=clock();
                        // printf("%d\n",thrnum);
                        // printf("%lu && %d\n",curTime, thrnum);
                        
                        //rs485_print_string("waitThrdSleep\n\");
                        
                        if(myThreads[thrnum].wakeupTime)
                            myThreads[thrnum].wakeupTime--;
                         
                          if(!myThreads[thrnum].wakeupTime)
                          {
                            //  printf("%d --> %d\n", currentThrnum, thrnum);
                             //   rs485_print_string("X waking up thread");
                             //   hex2ascii(thrnum,tmp);  rs485_print_string(tmp);
                             //   rs485_print_string("\n\r");
                            
                              currentThread=&(myThreads[thrnum]);
                              currentThrnum=thrnum;
                              mystack0=sp;
                              //printf("reload stacknum:%d for thread %d\n",currentThread->mystackCounter, currentThrnum);

                              while(currentThread->mystackCounter--){
                                      spval=currentThread->mystack[currentThread->mystackCounter];
                               //        printf("0x%08x<---0x%08x\n",sp,spval);
                                      
                                      asm("PUSH"); 
                                      TOSH=spval>>8;
                                      TOSL=spval &0xFF;
                                      
                              }

                        //      printf("stack is back at 0x%08x\n",sp);
                        //      printf("waking thread no %d\n",currentThrnum);

                            //   printf("stack is at 0x%08x \n",sp);

                              return;
                          }
                   
#endif
                        
                        }
            
            
                        }
         
         
         
         
         while(1);
         
         
    }
    
    
}


 
 
